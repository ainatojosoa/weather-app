export default interface Weather {
    api_key: string;
    url_base: string;
    query: string;
    weather: object;
  }